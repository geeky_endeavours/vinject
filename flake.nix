{
    description = "Setup for a dev env and easy build command for libvinject";

    inputs.nixpkgs.url = "github:nixos/nixpkgs";

    outputs = { self, nixpkgs }:
        let

            version = "0.0.0";

            supportedSystems = [ "x86_64-linux" ];

            forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

            nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
        in
        {
            packages = forAllSystems(system:
                let
                    pkgs = nixpkgsFor.${system};
                in
                {
                    default = pkgs.stdenv.mkDerivation rec {
                        pname = "libvinject";
                        version = "0.0.0";
                        src = self;
                        outputs = [ "out" ];

                        nativeBuildInputs = with pkgs; [
                          vala
                          meson 
                          glib
                          gobject-introspection
                          pkg-config
                          cmake
                          libgee
                          ninja
                          sqlite
                        ];
                        buildInputs = with pkgs; [
                          gmime3
                        ];
                        
                    };
                });
            
            devShells = forAllSystems(system:
                let
                    pkgs = nixpkgsFor.${system};
                in
                {
                    default = pkgs.mkShell {
                        packages = with pkgs; [
                            vala
                            vala-language-server
                            gobject-introspection
                            meson
                            cmake
                            libgee
                            pkg-config
                        ];
                    };
                });
        };
}