# Vinject 0.0.1

Vinject is a [Dependency Injection](https://en.wikipedia.org/wiki/Dependency_injection) container written in Vala for Vala. It was created to give Vala developers a method of easily configuring service instantiation whilst maintaining [IoC (Inversion of Control)](https://en.wikipedia.org/wiki/Inversion_of_control). Vinject uses constructor injection, via Vala construct properties and thus [GObject style construction](https://naaando.gitbooks.io/the-vala-tutorial/content/en/4-object-oriented-programming/gobject-style-construction.html). I currently have no plans to add other forms of injection.

Props to [Vladimir Lewandowski](https://github.com/vovaspace) who wrote [Brandi.js](https://brandi.js.org/). Brandi inspired the design of this project, and it is one of the best DI (Dependency Injection) Containers I have ever used, across typescript, C#, Python and more languages.

## Why?

If you are wondering: Why Should I use DI? what is IoC? etc. I strongly encourage you to read more about Dependency Injection and Inversion of Control (and maybe read about the SOLID principles in general). Dependency Injection helps you achieve these, making your code more loosely coupled and thereby easier to refactor, or modify the functionality of.

When we write OOP software, we generally try to adhere to the [SOLID](https://en.wikipedia.org/wiki/SOLID) principles. One of them is the *Inversion of Control* principle:

> A.  High-level modules should not import anything from low-level modules. Both should depend on abstractions (e.g., interfaces).

and

> B. Abstractions should not depend on details. Details (concrete implementations) should depend on abstractions.

(both are qoutes from wikipedia)

What this means is basically that your class, or method should not know the specific of how its dependencies work. You only define an interface that it takes as a parameter, and then that interface implements whatever functionality is depended on. This also plays well together with the Open and Closed principle, as you can "inject" a different implementation of the interfaces a class or function depends on, in order to change what exactly happens when the code is run. It also plays nice with the Single responsibility principle: Your class should only do one thing, and then depend on others to do other things.

**TL;DR:** DI is an easily configurable way of managing IoC

## Usage

I would encourage anyone to look at the docs, but a simple example on how this could be used is as follows:

### Simple example

Add a file to your project at this directory:

`<projectroot>/subprojects/libvinject.wrap`

With the following content:

```meson
[wrap-git]
url=https://gitlab.com/geeky_endeavours/vinject.git
revision=main
depth=1

[provide]
vinject-0.0.0 = vinject_dep
```

```vala
public interface ISample : Object {
    public abstract string display_string();
}
public class Sample: ISample, Object{
   public string field { get; construct; }
   public string display_string(){
      return "Showing: %s".printf(this.field);
   }
}
static int main (string[] arg) {
   var container = new Vinject.Injector();
   var example = new Vinject.ServiceToken<string>();
   var sample = new Vinject.ServiceToken<ISample>();

   container.register_constant(example, arg.length > 1 ? arg[1] : "Empty");
   container.register_transient<Sample, ISample>(
      sample, //The token for the ISample class
      field: example //A property name on the implementation of ISample, to initiate the value to be set by using the ServiceToken example
   );
   ISample sample_instance = container.obtain<ISample>(sample);
   message(sample_instance.display_string());
   return 0;
}
```

### Compiling

There are two ways to build this project

#### nix

simply run 
```bash
nix build
```

This will output in the `result` directory.

> NOTE: I am not very fluent in nix, and use it mostly with direnv to make developing projects more convenient.

#### meson

To compile using meson:

```bash
meson build # Create the build directory

cd build

meson compile # will compile the binary targets

meson test # will run all the unit tests

ninja doc # will generate the valadoc pages for this library

```

## Contributing

Anyone is welcome to fork this project and make a MR containing a new feature, or fixing a bug. I will review it when I have time, and merge or comment accordingly.

### Feature development

Any MR containing a feature should contain the following:

* A description of why this feature is needed and what it does
* An example of how to use it
* It must be documented with block comments (that the Vala doc generator can use)
* There must be unit tests showing that it works as described

### Bug fixes

Any MR for a bugfix should contain the following:

* A description of the bug
* A short description of the fix, and why it works this way
* A unit test that passes with the fix, but would fail without (more than one if relevant)

## Known issues:

Make sure to check the [Official git repository](https://gitlab.com/geeky_endeavours/vinject/-/issues/?sort=created_date&state=opened&label_name%5B%5D=bug&first_page_size=20) for any known issues.
