## [0.0.1](https://gitlab.com/geeky_endeavours/vinject/compare/v0.0.0...v0.0.1) (2024-02-13)


### Bug Fixes

* added missing closing bracket ([98fa7da](https://gitlab.com/geeky_endeavours/vinject/commit/98fa7da1e621b3546b9a036c6215d607044a3d1d))
