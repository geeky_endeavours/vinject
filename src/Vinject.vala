//  Vinject - A Dependency Injection container for Vala
//  Copyright (C) 2023  Rasmus Lindegaard

//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

using Gee;

namespace Vinject {

    public enum ServiceTypes {
        CONSTANT,
        SINGLETON,
        RESOLUTION,
        TRANSIENT
    }

    public class ServiceToken<T>: Object {

        public Type service_type {
            get {
                return typeof (T);
            }
        }

        private int _id;
        public int id {
            get {
                return this._id;
            }
        }

        public ServiceToken () {
            base ();
            this._id = counter++;
        }

        private static int counter = 0;

    }

    public errordomain VinjectErrors {
        NOT_INSTANTIABLE,
        NOT_DERIVED,
        NOT_REGISTERED,
        MISSING_PROPERTY_VALUE,
        DEPENDENCY_NOT_SERVICE_TOKEN,
        UNIMPLEMENTED_TYPE
    }

    public class Injector : Object {

        protected Gee.Map<int, ServiceTypes> type_map = new HashMap<int, ServiceTypes> ();
        protected Gee.Map<int, GLib.Value?> service_map = new HashMap<int, GLib.Value?> ();
        protected Gee.Map<int, Gee.Map<string, int>> dependency_map = new HashMap<int, Gee.Map<string, int>> ();
        protected Gee.Set<int> resolution_scoped_services = new Gee.HashSet<int> ();

        private Gee.Map<string, int> validate_dependencies (va_list dependencies) throws VinjectErrors {
            var debug_count = 0;
            var map = new Gee.HashMap<string, int> ();
            debug ("Validate dependencies: %d", debug_count++);
            while (true) {

                var property_name = dependencies.arg<string> ();
                debug ("Validate dependencies: %d", debug_count++);
                debug ("Propertyname is: %s", property_name);
                if (property_name == null) {
                    break;
                }
                debug ("Validate dependencies: %d", debug_count++);

                var property_type = dependencies.arg<ServiceToken> ();
                debug ("Validate dependencies: %d", debug_count++);

                if (property_type == null) {
                    throw new VinjectErrors.MISSING_PROPERTY_VALUE (
                        "There was no ServiceToken provided for the provided property: %s".printf (
                            property_name
                        )
                    );
                }
                debug (
                    "Validate dependencies: %d",
                    debug_count++
                );

                if (!GLib.Type.from_instance (property_type).is_a (typeof (ServiceToken))) {
                    throw new VinjectErrors.DEPENDENCY_NOT_SERVICE_TOKEN (
                        "Any dependencies given must be a ServiceToken"
                    );
                }
                debug (
                    "Validate dependencies: %d",
                    debug_count++
                );

                var token_id = ((ServiceToken)property_type).id;
                debug ("Validate dependencies: %d", debug_count++);

                if (!this.service_map.has_key (token_id)) {
                        throw new VinjectErrors.NOT_REGISTERED (
                            "No ServiceToken registered for %d typename: %s".printf (
                                token_id,
                                ((ServiceToken)property_type).service_type.name ()
                            )
                        );
                }

                debug ("Validate dependencies: %d", debug_count++);
                map.set (property_name, token_id);
                debug ("Validate dependencies: %d", debug_count++);
            }

            debug ("Validate dependencies: %d", debug_count++);
            return map;
        }

        private GLib.Value set_value<T> (T val) throws VinjectErrors {
            GLib.Value wrapper = Value (typeof (T));
            switch (typeof (T)) {
                case Type.BOOLEAN:
                    wrapper.set_boolean ((bool) val);
                    break;
                case Type.CHAR:
                    wrapper.set_schar ((int8) val);
                    break;
                case Type.DOUBLE:
                    wrapper.set_double ((double?) val);
                    break;
                case Type.FLOAT:
                    wrapper.set_float ((float?) val);
                    break;
                case Type.INT:
                    wrapper.set_int ((int) val);
                    break;
                case Type.INT64:
                    wrapper.set_int64 ((int64?) val);
                    break;
                case Type.LONG:
                    wrapper.set_long ((long) val);
                    break;
                case Type.OBJECT:
                    wrapper.take_object ((GLib.Object) val);
                    break;
                case Type.STRING:
                    wrapper.take_string ((string) val);
                    break;
                case Type.UCHAR:
                    wrapper.set_uchar ((uchar) val);
                    break;
                case Type.UINT:
                    wrapper.set_uint ((uint) val);
                    break;
                case Type.UINT64:
                    wrapper.set_uint64 ((uint64) val);
                    break;
                case Type.ULONG:
                    wrapper.set_ulong ((ulong) val);
                    break;
                default:
                    if (typeof (T).is_enum ()) {
                        wrapper.set_enum ((int) val);
                    } else if (typeof (T).is_instantiatable ()) {
                        wrapper.take_object ( (GLib.Object) val);
                        break;
                    } else {
                        throw new VinjectErrors.UNIMPLEMENTED_TYPE (
                            "Type: %s has not been implemented for injection".printf (
                                typeof (T).name ()
                            )
                        );
                    }

                    break;
            }

            return wrapper;
        }

        private T get_value<T> (Value val) throws VinjectErrors {
            switch (typeof (T)) {
                case Type.BOOLEAN:
                    return val.get_boolean ();
                case Type.CHAR:
                    return val.get_schar ();
                case Type.DOUBLE:
                    return (double?)val.get_double ();
                case Type.FLOAT:
                    return (float?)val.get_float ();
                case Type.INT:
                    return val.get_int ();
                case Type.INT64:
                    return (int64?) val.get_int64 ();
                case Type.LONG:
                    return val.get_long ();
                case Type.OBJECT:
                    return val.get_object ();
                case Type.STRING:
                    return val.get_string ();
                case Type.UCHAR:
                    return val.get_uchar ();
                case Type.UINT:
                    return val.get_uint ();
                case Type.UINT64:
                    return val.get_uint64 ();
                case Type.ULONG:
                    return val.get_ulong ();
                default:
                    if (typeof (T).is_enum ()) {
                        return val.get_enum ();
                    } else if (typeof (T).is_a (typeof (Object))) {
                        return val.get_object ();
                    } else {
                        throw new VinjectErrors.UNIMPLEMENTED_TYPE (
                            "Type: %s has not been implemented for injection".printf (
                                typeof (T).name ()
                            )
                        );
                    }
            }
        }

        private void register<TImplementation, TInterface> (
            ServiceToken<TInterface>token,
            ServiceTypes service_type,
            Gee.Map<string, int> dependencies
        ) throws VinjectErrors {

            Type implementation_type = typeof (TImplementation);
            Type interface_type = typeof (TInterface);

            if (!implementation_type.is_a (interface_type)) {
                throw new VinjectErrors.NOT_DERIVED (
                    "TImplementation '%s' is not derived from TInterface '%s'".printf (
                        implementation_type.name (),
                        interface_type.name ()
                    )
                );
            }

            if (!implementation_type.is_object ()) {
                throw new VinjectErrors.NOT_INSTANTIABLE (
                    "The TImplementation passed to Injector.register must be instantiable, %s is not".printf (
                        typeof (TImplementation).name ()
                    )
                );
            }

            this.type_map.set (token.id, service_type);
            this.service_map.set (token.id, Value (implementation_type));
            this.dependency_map.set (token.id, dependencies);

        }

        public void register_transient<TImplementation, TInterface> (
            ServiceToken<TInterface> token,
            ...
        ) throws VinjectErrors {

            var dependencies = va_list ();
            var mapped_dependencies = validate_dependencies (dependencies);

            this.register<TImplementation, TInterface> (token, Vinject.ServiceTypes.TRANSIENT, mapped_dependencies);

        }

        public void register_singleton<TImplementation, TInterface> (
            ServiceToken<TInterface> token,
            ...
        ) throws VinjectErrors {

            var dependencies = va_list ();
            var mapped_dependencies = validate_dependencies (dependencies);

            this.register<TImplementation, TInterface> (token, Vinject.ServiceTypes.SINGLETON, mapped_dependencies);
        }

        public void register_resolution<TImplementation, TInterface> (
            ServiceToken<TInterface> token,
            ...
        ) throws VinjectErrors {

            var dependencies = va_list ();
            var mapped_dependencies = validate_dependencies (dependencies);

            this.register<TImplementation, TInterface> (token, Vinject.ServiceTypes.RESOLUTION, mapped_dependencies);
        }

        public void register_constant<T> (ServiceToken<T> token, T constant) throws VinjectErrors {
            var reg = 0;
            debug ("reg constant %d", reg++);
            this.type_map.set (token.id, ServiceTypes.CONSTANT);
            debug ("reg %d", reg++);

            var constant_val = this.set_value (constant);
            debug ("reg constant %d", reg++);

            this.service_map.set (token.id, constant_val);
            debug ("reg constant %d", reg++);


        }

        private int _depth = 0;
        /**
         * Determines whether we are still in the same resolution scope.
         * A value over 1 tells us that resolution scoped services should not be
         * reinitialized
        */
        private int depth {
            get {
                return this._depth;
            }
            private set {
                if (value == 0) {
                    foreach (var tokenid in this.resolution_scoped_services) {
                        var val = this.service_map.get (tokenid);
                        val.reset ();
                        this.service_map.set (tokenid, val);
                    }
                    this.resolution_scoped_services.clear ();
                }
                this._depth = value;
                debug ("depth set to: %d".printf (value));
            }
        }

        private Value inject_service (int tokenid, string? service_name = null) throws VinjectErrors {
            this.depth++;

            ServiceTypes? service_type = this.type_map.get (tokenid);


            if (service_type == null) {
                this.depth--;
                throw new VinjectErrors.NOT_REGISTERED (
                    "No registered service for servicetoken %d with type: %s found".printf (
                        tokenid,
                        service_name
                    )
                );
            }

            Value service_val = this.service_map.get (tokenid);

            debug ("ServiceVal typename: %s", service_val.type ().name ());

            if (service_type == ServiceTypes.CONSTANT) {
                this.depth--;
                return service_val;
            }else if (service_type == ServiceTypes.RESOLUTION || service_type == ServiceTypes.SINGLETON) {
                if (service_val.get_object () != null) {
                    this.depth--;
                    return service_val;
                }
            }

            service_val.reset ();

            var deps = this.dependency_map.get (tokenid);
            var propnames = new string[deps.keys.size];
            var propvalues = new Value[deps.keys.size];
            var index = 0;

            foreach ( var entry in deps ) {
                propnames[index] = entry.key;
                var val = this.inject_service (entry.value);
                propvalues[index] = val;

                debug ("prop: %s val type: %s".printf (entry.key, val.type_name ()));
                index++;
            }


            var service_instance = Object.new_with_properties (service_val.type (), propnames, propvalues);
            service_val.take_object (service_instance);
            this.service_map.set (tokenid, service_val);

            if (service_type == ServiceTypes.RESOLUTION) {
                this.resolution_scoped_services.add (tokenid);
            }

            this.depth--;
            return service_val;
        }

        /**
         * Obtain the service bound by the specified token. Services are initialized
         * according to their specified service types
         *
         * @param token The service token for which we wish to obtain an instance
         * @return The registered service implementation
        */
        public T obtain<T> (ServiceToken<T> token) throws VinjectErrors {

            var raw_value = this.inject_service (token.id, token.service_type.name ());

            return this.get_value<T> (raw_value);
        }

    }
}
