namespace Vinject.Test {
    /**
    * Tests that a Transient service A can be registered and obtained, with
    * transient deps B, C and D each is instantiated each time it is needed
    */
    public class UnitTest : IUnitTest, Object {

        public override void test () {

            var main_service = this.injector.obtain (this.main_token);

            var str = main_service.to_string ();

            message (str);

            assert (str == "D4B5D0D1B2C3A6");
        }

        public override string name { get; construct set; }

        public ServiceToken<IServiceA> main_token { get; set; }

        public Injector injector { get; set; }

        construct {
            this.name = "ObtainServiceWithDeptree3";

            this.injector = new Injector ();

            var d_token = new ServiceToken<IServiceD> ();
            var c_token = new ServiceToken<IServiceC> ();
            var b_token = new ServiceToken<IServiceB> ();

            this.main_token = new ServiceToken<IServiceA> ();

            this.injector.register_transient<ServiceD, IServiceD> (d_token);

            this.injector.register_transient<ServiceB, IServiceB> (b_token, d: d_token);

            this.injector.register_transient<ServiceC, IServiceC> (c_token, b: b_token, d: d_token);

            this.injector.register_transient<ServiceA, IServiceA> (this.main_token, b: b_token, c: c_token);

        }

    }

}
