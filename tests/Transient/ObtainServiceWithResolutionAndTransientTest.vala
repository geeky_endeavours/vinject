namespace Vinject.Test {
    /**
    * Tests that a Transient service A can be registered and obtained, with dependency B as resolution
    * meaning it is instantiated once per call to obtain and injected into any service that depends on it.
    */
    public class UnitTest : IUnitTest, Object {

        public override void test () {

            var main_service1 = this.injector.obtain (this.main_token);
            var main_service2 = this.injector.obtain (this.main_token);


            assert (main_service1.to_string () == "D1B2D0D1B2C3A4");
            assert (main_service2.to_string () == "D6B7D5D6B7C8A9");
        }

        public override string name { get; construct set; }

        public ServiceToken<IServiceA> main_token { get; set; }

        public Injector injector { get; set; }

        construct {
            this.name = "ObtainTransientServiceWithResolutionAndTransient";

            this.injector = new Injector ();

            var d_token = new ServiceToken<IServiceD> ();
            var c_token = new ServiceToken<IServiceC> ();
            var b_token = new ServiceToken<IServiceB> ();

            this.main_token = new ServiceToken<IServiceA> ();

            this.injector.register_transient<ServiceD, IServiceD> (d_token);

            this.injector.register_resolution<ServiceB, IServiceB> (b_token, d: d_token);

            this.injector.register_transient<ServiceC, IServiceC> (c_token, b: b_token, d: d_token);

            this.injector.register_transient<ServiceA, IServiceA> (this.main_token, b: b_token, c: c_token);

        }

    }

}
