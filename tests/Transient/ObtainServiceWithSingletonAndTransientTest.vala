namespace Vinject.Test {
    /**
    * Tests that a Transient service A can be registered and obtained, with dependency C as a singleton, meaning once it
    * has been instantiated the same instantiation is used across every call to obtain, while all other dependencies are transient.
    */
    public class UnitTest : IUnitTest, Object {

        public override void test () {

            var main_service1 = this.injector.obtain (this.main_token);
            var main_service2 = this.injector.obtain (this.main_token);

            assert (main_service1.to_string () == "D4B5D0D1B2C3A6");
            assert (main_service2.to_string () == "D7B8D0D1B2C3A9");
        }

        public override string name { get; construct set; }

        public ServiceToken<IServiceA> main_token { get; set; }

        public Injector injector { get; set; }

        construct {
            this.name = "ObtainTransientServiceWithSingletonAndTransient";

            this.injector = new Injector ();

            var d_token = new ServiceToken<IServiceD> ();
            var c_token = new ServiceToken<IServiceC> ();
            var b_token = new ServiceToken<IServiceB> ();

            this.main_token = new ServiceToken<IServiceA> ();

            this.injector.register_transient<ServiceD, IServiceD> (d_token);

            this.injector.register_transient<ServiceB, IServiceB> (b_token, d: d_token);

            this.injector.register_singleton<ServiceC, IServiceC> (c_token, b: b_token, d: d_token);

            this.injector.register_transient<ServiceA, IServiceA> (this.main_token, b: b_token, c: c_token);

        }

    }

}
