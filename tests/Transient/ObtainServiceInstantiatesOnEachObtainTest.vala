namespace Vinject.Test {
    /**
    * Tests that a Transient service D can be registered and obtained, and is reinstantiated every time
    */
    public class UnitTest : TestBase<IServiceD>, IUnitTest {

        public override void test () {

            this.injector.register_transient<ServiceD, IServiceD> (this.token);

            var service_instance1 = this.injector.obtain (this.token);
            var service_instance2 = this.injector.obtain (this.token);

            assert (service_instance1.generate_d_spec_value () == "D0");
            assert (service_instance2.generate_d_spec_value () == "D1");

        }

        construct {
            this.name = "ObtainTransientServiceInstantiatesOnEachObtain";
        }

    }

}
