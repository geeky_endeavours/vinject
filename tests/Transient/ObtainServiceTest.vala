namespace Vinject.Test {
    /**
    * Tests that a Transient service D can be registered and obtained
    */
    public class UnitTest : TestBase<IServiceD>, IUnitTest {

        public override void test () {

            this.injector.register_transient<ServiceD, IServiceD> (this.token);

            var service_instance = this.injector.obtain (this.token);

            var str = service_instance.generate_d_spec_value ();

            assert (str == "D0");

        }

        construct {
            this.name = "ObtainTransientService";
        }

    }

}
