namespace Vinject.Test {

    public interface IUnitTest : Object {

        public abstract string name { get; construct set; }
        public abstract void test ();

    }

}
