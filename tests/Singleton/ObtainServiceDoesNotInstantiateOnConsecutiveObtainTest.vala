namespace Vinject.Test {
    /**
    * Tests that a Singleton service D can be registered and obtained, consecutive calls to obtain
    * returns the same instance of service D
    */
    public class UnitTest : TestBase<IServiceD>, IUnitTest {

        public override void test () {

            this.injector.register_singleton<ServiceD, IServiceD> (this.token);

            var service_instance1 = this.injector.obtain (this.token);
            var service_instance2 = this.injector.obtain (this.token);

            assert (service_instance1.generate_d_spec_value () == "D0");
            assert (service_instance2.generate_d_spec_value () == "D0");

        }

        construct {
            this.name = "ObtainSingletonServiceDoesNotInstantiateOnConsecutiveObtain";
        }

    }

}
