namespace Vinject.Test {
    /**
    * Tests that a Singleton service B can be registered and obtained with dependency: singleton D
    */
    public class UnitTest : IUnitTest, Object {

        public override void test () {

            var main_service = this.injector.obtain (this.main_token);

            var str = main_service.generate_b_spec_value ();

            message (str);

            assert (str == "D0B1");
        }

        public override string name { get; construct set; }

        public ServiceToken<IServiceB> main_token { get; set; }

        public Injector injector { get; set; }

        construct {
            this.name = "ObtainSingletonServiceWithDeptree2";

            this.injector = new Injector ();

            var sub_service_token = new ServiceToken<IServiceD> ();
            this.main_token = new ServiceToken<IServiceB> ();

            this.injector.register_singleton<ServiceD, IServiceD> (sub_service_token);

            this.injector.register_singleton<ServiceB, IServiceB> (this.main_token, d: sub_service_token);

        }

    }

}
