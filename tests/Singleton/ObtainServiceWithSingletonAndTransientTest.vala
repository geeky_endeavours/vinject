namespace Vinject.Test {
    /**
    * Tests that a Singleton service A can be registered and obtained
    * with the only non-singleton dependency being D, as it is a transient, and it is instantiated each time it is needed.
    */
    public class UnitTest : IUnitTest, Object {

        public override void test () {

            var main_service1 = this.injector.obtain (this.main_token);
            var main_service2 = this.injector.obtain (this.main_token);


            assert (main_service1.to_string () == "D1B2D0D1B2C3A4");
            assert (main_service2.to_string () == "D1B2D0D1B2C3A4");
        }

        public override string name { get; construct set; }

        public ServiceToken<IServiceA> main_token { get; set; }

        public Injector injector { get; set; }

        construct {
            this.name = "ObtainSingletonServiceWithSingletonAndTransient";

            this.injector = new Injector ();

            var d_token = new ServiceToken<IServiceD> ();
            var c_token = new ServiceToken<IServiceC> ();
            var b_token = new ServiceToken<IServiceB> ();

            this.main_token = new ServiceToken<IServiceA> ();

            this.injector.register_transient<ServiceD, IServiceD> (d_token);

            this.injector.register_singleton<ServiceB, IServiceB> (b_token, d: d_token);

            this.injector.register_singleton<ServiceC, IServiceC> (c_token, b: b_token, d: d_token);

            this.injector.register_singleton<ServiceA, IServiceA> (this.main_token, b: b_token, c: c_token);

        }

    }

}
