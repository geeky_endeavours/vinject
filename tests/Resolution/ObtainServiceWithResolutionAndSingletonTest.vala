namespace Vinject.Test {

    /**
    * Tests that a resolution service A can be obtained with the complete, appropriate dependency tree
    * and that its dependency B registered as a transient is reinstantiated each time it is needed, while C
    * registered as a singleton is only instantiated once, and D is instantiated only once per call of obtain.
    */
    public class UnitTest : IUnitTest, Object {

        public override void test () {

            var main_service1 = this.injector.obtain (this.main_token);
            var main_service2 = this.injector.obtain (this.main_token);

            assert (main_service1.to_string () == "D0B3D0D0B1C2A4");
            assert (main_service2.to_string () == "D5B6D0D0B1C2A7");
        }

        public override string name { get; construct set; }

        public ServiceToken<IServiceA> main_token { get; set; }

        public Injector injector { get; set; }

        construct {
            this.name = "ObtainResolutionServiceWithResolutionAndSingleton";

            this.injector = new Injector ();

            var d_token = new ServiceToken<IServiceD> ();
            var c_token = new ServiceToken<IServiceC> ();
            var b_token = new ServiceToken<IServiceB> ();

            this.main_token = new ServiceToken<IServiceA> ();

            this.injector.register_resolution<ServiceD, IServiceD> (d_token);

            this.injector.register_transient<ServiceB, IServiceB> (b_token, d: d_token);

            this.injector.register_singleton<ServiceC, IServiceC> (c_token, b: b_token, d: d_token);

            this.injector.register_resolution<ServiceA, IServiceA> (this.main_token, b: b_token, c: c_token);

        }

    }

}
