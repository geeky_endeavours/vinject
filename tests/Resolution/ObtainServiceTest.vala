namespace Vinject.Test {

    /**
    * Tests that a simple service registered as resolution is can be obtained
    */
    public class UnitTest : TestBase<IServiceD>, IUnitTest {

        public override void test () {

            this.injector.register_resolution<ServiceD, IServiceD> (this.token);

            var service_instance = this.injector.obtain (this.token);

            var str = service_instance.generate_d_spec_value ();

            assert (str == "D0");
        }

        construct {
            this.name = "ObtainResolutionService";
        }

    }

}
