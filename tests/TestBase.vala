namespace Vinject.Test {

    public abstract class TestBase<T> : Object {

        protected Vinject.Injector injector { get; set; }

        protected Vinject.ServiceToken<T> token { get; set; }

        public string name { get; construct set; }

        public T exp_val { get; construct; }

        construct {

            this.injector = new Vinject.Injector ();
            this.token = new Vinject.ServiceToken<T> ();
        }

        public abstract void test ();

    }

}
