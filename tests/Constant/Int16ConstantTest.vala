//  Vinject tests - A Dependency Injection container for Vala
//  Copyright (C) 2023  Rasmus Lindegaard

//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace Vinject.Test {

    /**
    * Tests that an int16 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<int16>{

        public UnitTest () {
            Object (
                name: "int16",
                exp_val: int16.MAX
            );
        }

    }

}
