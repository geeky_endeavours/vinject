//  Vinject tests - A Dependency Injection container for Vala
//  Copyright (C) 2023  Rasmus Lindegaard

//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace Vinject.Test {

   /**
    * Tests that an int64 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<int64?>{

        public UnitTest () {
            Object (
                name: "int64",
                exp_val: int64.MAX
            );
        }

        public override void test () {
            var obtained_constant = this.injector.obtain (this.token);
            assert (obtained_constant == (int64)this.exp_val);
        }

    }

}
