//  Vinject tests - A Dependency Injection container for Vala
//  Copyright (C) 2023  Rasmus Lindegaard

//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace Vinject.Test {

    /**
    * This abstract class forms the basis of tests for registering and obtaining
    * simple data types
    */
    public abstract class ConstantBase<T> : TestBase<T>, IUnitTest {

        private string _name;

        public override string name { get { return this._name; } construct set {
            this._name = "Constant/%s".printf (value);
        }}

        construct {
            this.injector.register_constant (this.token, this.exp_val);
        }

        public override void test () {
            var obtained_constant = this.injector.obtain (this.token);
            assert (obtained_constant == this.exp_val);
        }
    }

}
