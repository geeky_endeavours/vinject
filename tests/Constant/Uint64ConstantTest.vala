namespace Vinject.Test {

    /**
    * Tests that an uint64 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<uint64?>{

        public UnitTest () {
            Object (
                name: "uint64",
                exp_val: uint64.MAX
            );
        }

    }

}
