namespace Vinject.Test {

    /**
    * Tests that an uint registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<uint?>{

        public UnitTest () {
            Object (
                name:"Uint",
                exp_val: uint.MAX
            );
        }

    }

}
