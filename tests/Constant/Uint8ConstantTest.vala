namespace Vinject.Test {

    /**
    * Tests that an uint8 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<uint8>{

        public UnitTest () {
            Object (
                name: "uint8",
                exp_val: uint8.MAX
            );
        }

    }

}
