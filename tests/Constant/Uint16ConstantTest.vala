namespace Vinject.Test {

    /**
    * Tests that an uint16 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<uint16>{

        public UnitTest () {
            Object (
                name: "uint16",
                exp_val: uint16.MAX
            );
        }

    }

}
