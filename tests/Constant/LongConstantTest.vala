namespace Vinject.Test {

    /**
    * Tests that a long registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<long>{

        public UnitTest () {
            Object (
                name:"Long",
                exp_val: long.MAX
            );
        }

    }

}
