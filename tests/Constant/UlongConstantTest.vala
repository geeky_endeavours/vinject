namespace Vinject.Test {

    /**
    * Tests that an ulong registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<ulong>{

        public UnitTest () {
            Object (
                name:"Ulong",
                exp_val: long.MAX
            );
        }

    }

}
