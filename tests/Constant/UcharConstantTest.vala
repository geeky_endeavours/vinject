namespace Vinject.Test {

    /**
    * Tests that an uchar registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<uchar>{

        public UnitTest () {
            Object (
                name: "uchar",
                exp_val: 'A'
            );
        }

    }

}
