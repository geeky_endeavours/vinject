namespace Vinject.Test {

    /**
    * Tests that an uint32 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : ConstantBase<uint32>{

        public UnitTest () {
            Object (
                name: "uint32",
                exp_val: uint32.MAX
            );
        }

    }

}
