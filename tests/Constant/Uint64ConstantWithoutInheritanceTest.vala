//  Vinject tests - A Dependency Injection container for Vala
//  Copyright (C) 2023  Rasmus Lindegaard

//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace Vinject.Test {

    /**
    * Tests that an uint64 registered as constant is
    * obtained with the expected value
    */
    public class UnitTest : Object, IUnitTest {

        public ServiceToken<uint64?> token { get; set; }
        public Injector injector { get; set; }

        public uint64? exp_value { get; set; }

        public string name { get; construct set; }

        public void test () {
            var i = this.injector.obtain (this.token);

            debug (@"Got value: $i == $(this.exp_value) ? $(this.exp_value == i)");

            assert ( i == (uint64)this.exp_value );

        }


        construct {
            this.name = "Uint64WithoutInheritance";
            this.exp_value = uint64.MAX;

            this.token = new ServiceToken<uint64?> ();
            this.injector = new Injector ();

            this.injector.register_constant (this.token, this.exp_value);
        }

    }

}
