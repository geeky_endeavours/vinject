namespace Vinject.Test {

    public class ServiceA : ServiceBase, IServiceA {

        public IServiceB b { get; construct; }
        public IServiceC c { get; construct; }

        public string generate_a_spec_value () {
            return @"A$id";
        }

        public string to_string () {
            return @"$(b)$(c)$(generate_a_spec_value ())";
        }
    }
}
