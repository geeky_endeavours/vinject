namespace Vinject.Test {

    public interface IServiceC : Object {

        public abstract string generate_c_spec_value ();

        public abstract string to_string ();
    }

}
