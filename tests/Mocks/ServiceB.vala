namespace Vinject.Test {

    public class ServiceB : ServiceBase, IServiceB {

        public IServiceD d { get; construct; }

        public string generate_b_spec_value () {
            return @"$(d)B$id";
        }

        public override string to_string () {
            return this.generate_b_spec_value ();
        }
    }

}
