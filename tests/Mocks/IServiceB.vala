namespace Vinject.Test {

    public interface IServiceB : Object {

        public abstract string generate_b_spec_value ();

        public abstract string to_string ();
    }

}
