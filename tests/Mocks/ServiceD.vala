namespace Vinject.Test {

    public class ServiceD : ServiceBase, IServiceD {

        public string generate_d_spec_value () {
            return @"D$id";
        }

        public override string to_string () {
            return this.generate_d_spec_value ();
        }
    }

}
