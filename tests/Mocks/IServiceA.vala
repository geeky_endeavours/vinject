namespace Vinject.Test {

    public interface IServiceA : Object {

        public abstract string generate_a_spec_value ();

        public abstract string to_string ();
    }

}
