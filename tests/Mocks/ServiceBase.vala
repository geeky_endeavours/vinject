namespace Vinject.Test {

    public abstract class ServiceBase : Object {

        protected static int counter = 0;

        public int id { get; protected set; }

        construct {
            this.id = ServiceBase.counter++;
        }

    }

}
