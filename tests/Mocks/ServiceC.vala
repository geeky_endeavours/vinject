namespace Vinject.Test {

    public class ServiceC : ServiceBase, IServiceC {

        public IServiceD d { get; construct; }

        public IServiceB b { get; construct; }

        public string generate_c_spec_value () {
            return @"$(d)$(b)C$id";
        }

        public override string to_string () {
            return this.generate_c_spec_value ();
        }

    }

}
