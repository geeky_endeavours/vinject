namespace Vinject.Test {

    public interface IServiceD : Object {

        public abstract string generate_d_spec_value ();

        public abstract string to_string ();
    }
}
