namespace Vinject.Test {

    /**
    * Tests that service tokens are generated with different IDs
    */
    public class UnitTest : Object, IUnitTest {

        public ServiceToken<string> token1;
        public ServiceToken<int> token2;

        public override void test () {

            assert (this.token1.id != this.token2.id);

        }

        public override string name { get; construct set;}

        construct {
            this.name = "ServiceTokenId";

            this.token1 = new ServiceToken<string> ();
            this.token2 = new ServiceToken<int> ();
        }

    }

}
