namespace Vinject.Test {

    int main (string[] args) {

        GLib.Test.init (ref args);

        test = new UnitTest ();

        GLib.Test.add_func ("/%s".printf (test.name), run_test);

        return GLib.Test.run ();

    }

    static IUnitTest test;

    public void run_test () {
        test.test ();
    }

}
